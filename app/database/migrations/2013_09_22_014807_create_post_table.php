<?php

use Illuminate\Database\Migrations\Migration;

class CreatePostTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('post', function($table)
        {
            $table->increments('id');
            $table->string("title");
            $table->integer("category_id");
            $table->integer("subcategory_id");
            $table->integer("seller_id");
            $table->integer("quantity");
            $table->integer("location_id");
            $table->integer("range");
            $table->float("cost");
            $table->string("seller_name");
            $table->string("seller_email");
            $table->string("seller_phone");
            $table->text("description");
            $table->integer("condition_id");
            $table->integer("status_id");
            $table->integer("views");
            $table->integer("bids");
            $table->integer("feature_id");
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop("post");
	}

}