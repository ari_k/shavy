<?php

use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('categories', function($table)
        {
            $table->increments('id');
            $table->string("name");
            $table->boolean("show");
            $table->string("svg");
            $table->string("img");
            $table->integer("display_order");
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop("categories");
	}

}