<?php

use Illuminate\Database\Migrations\Migration;

class CreateSubcategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('sub_categories', function($table)
        {
            $table->increments('id');
            $table->integer("cat_id");
            $table->string("name");
            $table->boolean("show");
            $table->string("svg");
            $table->string("img");
            $table->integer("display_order");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("sub_categories");
    }

}