<?php


class UserController extends BaseController{


    public function index(){
        $title="Welcome";
        $message=$this->database_message();
        $username="shavyg2";

        return View::make('user.home')->with("title",$title." ".$username)
            ->with('message',$message)
            ->with('username',$username)
            ->with("active_home",true);
    }


    public function database_message(){
        return "Hello World";
    }
}
