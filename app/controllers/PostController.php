<?php



class PostController extends BaseController
{

    public function post_ad()
    {
        $title="Create A Post";
        return View::make("post.post_ad")->with('title',$title)
        ->with("active_create_post",true);
    }

    public function save_post(){
        $post=Input::all();
        unset($post['_token']);
        $validation=new PostValidation($post);

        if($validation->passes()){
            Post::create($post);
            echo "saved to db";
        }else{
            return Redirect::route('post_ad')->withInput();
        }

    }


    public function view_ads()
    {
        $posts = $this->get_ads();
	$title = "Browse Ads";
        return View::make("post.all_post")->with("posts", $posts)
        ->with("title",$title)
        ->with("active_browse_ads",true);
    }

    private function get_ads()
    {
        
		return Post::all();
		return array(
            $this->get_object(),
            $this->get_object(),
            $this->get_object(),
            $this->get_object(),
            $this->get_object(),
            $this->get_object(),
            $this->get_object()
        );
    }

    private function get_object(){
        return (object)array(
            "name"=>"Super Bike",        
            "description"=>"An Amazing bike to ride and feel the wind in your hair. It has a really good suspension and is really easy to ride. Perfect balancing and is shaped sexy as hell!!!",
            "cost"=>999.99,
            "seller"=>"Charles Ramzie",
            "image"=>"http://www.timeless-touring.com/images/bikes/yamaha-r6-06.jpg",
            "location"=>"Toronto"
        );
    }



    public function single_ad(){
        //data from db
        $ad=$this->get_ad();

        return View::make("post.detail")
            ->with("ad",$ad)
            ->with("title",$ad->title);
    }

    private function get_ad(){
        return (object) array(
            "title"=>"Super Bike",
            "description"=>"Hey Guys look at my new add",
            "cost"=>999.99,
            "contact"=>"Charles",
            "image"=>"http://www.timeless-touring.com/images/bikes/yamaha-r6-06.jpg"
        );
    }
}
