<?php

Route::get('/', array("uses" => "UserController@index", "as" => "homepage"));


Route::get('/post/create', array("uses" => "PostController@post_ad", "as" => "post_ad"));
Route::post('/post/create', array("uses" => "PostController@save_post", "as" => "create_post"));


Route::get('/post/all', array("uses" => "PostController@view_ads", "as" => "view_ads"));

Route::get('/post/detail', array("uses" => "PostController@single_ad", "as" => "single_ad"));


Route::get('/help', function () {
    echo "Welcome Friend";
    echo "<br>";
    $icons = new Iconpack();

    echo $icons->camera(200, 200, "red");
    echo "<br>";

    $icons->all_icons_name();
});

