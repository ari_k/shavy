<!doctype html>
<html lang="en-US">
<head>
<!--    google jquery-->
    {{HTML::script('http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js')}}


<!--    twitter bootstrap-->
    {{HTML::style('bootstrap/dist/css/bootstrap.min.css')}}
    {{HTML::script('bootstrap/dist/js/bootstrap.min.js')}}

<!--    Slider by james smith-->
    {{HTML::style('slider/css/simple-slider.css')}}
    {{HTML::style('slider/css/simple-slider-volume.css')}}
    {{HTML::script('slider/js/simple-slider.min.js')}}


    <meta charset="UTF-8">
    <title>
        @if(isset($title))
        {{$title}}
        @endif
    </title>

    <style>
        body{
            max-width: 100%;
            overflow-x: hidden;
            margin-bottom:100px;
        }
    </style>
</head>
<body>

<br>

<div class="container">
    @include('nav.menu')
</div>

<div class="" style="padding-left:10px;padding-right:10px;">
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            @if(isset($title))
            <h1>{{ucwords($title)}}</h1>
            @endif
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <nav>
                @include("nav.sidebar")
            </nav>
        </div>
        <div class="col-md-8">

            @yield('page')
        </div>
        <div class="col-md-2">

        </div>
    </div>
</div>


</body>
</html>
