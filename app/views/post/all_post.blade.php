@extends("layout.master")


@section("page")
    @if(isset($posts))
<div class="container">
        <div class="row">
            @foreach($posts as $post)
              @include("post.single_post")
            @endforeach
        </div>
</div>
    @endif
@stop
