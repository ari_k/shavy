@extends("layout.master")


@section("page")
<?php echo Form::open(array('url' => URL::route('create_post'), 'method' => 'create', 'role' => 'form')); ?>

<div class="form-group">

    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
            {{Form::label('seller_name',"Seller's Name")}}

            {{Form::text('seller_name',Input::old('seller_name'),array(
            'class'=>'form-control',
            'placeholder'=>'Enter your name'
            ))}}

        </div>

        <div class="col-xs-12 col-sm-6 col-md-6">
            {{Form::label('seller_email',"Seller's Email")}}
            <small>( Required )</small>


            {{ Form::input('text','seller_email',Input::old('email'),
            array(
            'class'=>'form-control',
            'placeholder'=>'Enter your email',
            'required'=>''
            ))
            }}
        </div>
    </div>
</div>

<br>

<div class="form-group">

    <div class="row">
        <div class="col-sm-6 col-md-6">
            {{Form::label('title',"Item Name")}}
            <small>( Required )</small>

            {{ Form::input('text','title',Input::old('name'),
            array(
            'class'=>'form-control',
            'placeholder'=>'Enter Item Name',
            "required"=>''
            ))
            }}
        </div>

        <div class="col-xs-12 col-sm-5 col-md-4 pull-right">
            <label>
                $ Cost
                <small>*Required</small>
            </label>
            {{Form::input('text','cost',Input::old('cost'),array(
            'class'=>'form-control',
            'placeholder'=>'10.00',
            'pattern'=>'[0-9\.]*',
            'required'=>''
            ))}}
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-6">
            {{Form::label('description',"Description")}}
            <small>( Required )</small>

        </div>
        <div class="col-md-12">

            {{Form::textarea('description',Input::old('description'),
            array(
            'rows'=>'8',
            'class'=>'form-control',
            'required'=>''
            ))}}
        </div>

        <br>


        <div class="col-xs-6 col-md-6">

            {{Form::label('image',"Image")}}

            {{Form::file('image',array())}}
        </div>

        <div class="col-xs-4 col-md-3 pull-right">
            {{Form::label('quantity',"Quantity")}}

            {{Form::input('number','quantity',Input::old('quantity')|1,
            array(
            'class'=>'form-control',
            'required'=>''
            ))}}
        </div>


    </div>

    <br>
    <br>


    <div class="row">
        <div class="col-md-10">
            <label>Distance</label>
            {{
            Form::input('text','range',Input::old('range')|0,array('id'=>'range_slider','data-slider'=>'true','data-slider-theme'=>'volume','data-slider-range'=>'0,500','data-slider-step'=>'5'))}}
            <script>
                $(document).ready(function () {
                    $("#range_slider").bind("slider:changed", function (event, data) {
                        $("#range_display").html(data.value);
                        $("#range_slider")[0].value = data.value;
                    });
                });
            </script>
        </div>
        <br>

        <div class="col-md-2">
            <label id="range_display">
                @if(Input::old('range')!=null)
                {{Input::old('range')}}
                @else
                0
                @endif
            </label> km
        </div>

    </div>

    <br>
</div>
<div class="row">
    <div class="col-md-6">
        {{Form::label("Category")}}
        @if(isset($categories))
        {{ Form::select('category_id',$categories,Input::old('category_id'),array('class'=>'form-control')) }}
        @else
        {{
        Form::select('category_id',array('1'=>'Clothing','2'=>'Vehicles','3'=>'Workers','4'=>'Occupation'),Input::old('category_id'),array('class'=>'form-control'))
        }}
        @endif
    </div>
    <div id="subcategory" class="col-md-6">

    </div>
</div>
<br>
<div class="row">
    <div class="col-md-6">
        {{Form::label("Condition")}}
        @if(isset($condition))
        {{ Form::select('condition_id',$condition,Input::old('condition_id'),array('class'=>'form-control')) }}
        @else
        {{
        Form::select('condition_id',array('1'=>'New','2'=>'Used','3'=>'Refurbished','4'=>'Other'),Input::old('condition_id'),array('class'=>'form-control'))
        }}
        @endif
    </div>
    <div class="col-md-6">
        {{Form::label("Post Type")}}
        @if(isset($feature))
        {{ Form::select('feature_id',$feature,Input::old('feature_id'),array('class'=>'form-control')) }}
        @else
        {{ Form::select('feature_id',array('1'=>'Free','2'=>'Gold &nbsp;&nbsp;($6.99)','3'=>'Silver &nbsp;($2.99)','4'=>'Bronze
        ($0.50)'),Input::old('feature_id'),array('class'=>'form-control')) }}
        @endif
    </div>
</div>

<br>
<div class="row">


</div>
<br>
<div class="row">
    <div class="col-md-4 col-md-offset-8">
        <label>
            Phone Number
            <small>(Optional)</small>
        </label>
        {{Form::input('tel','seller_phone',Input::old('seller_phone'),
        array(
        'class'=>'form-control',
        'placeholder'=>'(123) 456-7890'
        ))
        }}
    </div>
</div>

<br>

<div class="row">
    <div class="col-md-offset-8 col-md-4">

        <div class="checkbox">
            <label>
                <!--                <input type="checkbox" value="">-->
                @if(Input::old('kijiji')=='on')
                {{Form::input('checkbox','kijiji',null,array('checked'=>'true'))}}
                @else
                {{Form::input('checkbox','kijiji')}}
                @endif
                Post To Kijiji
            </label>
        </div>
    </div>
</div>

<button type="submit" class="btn">Submit</button>
{{Form::close()}}

@stop