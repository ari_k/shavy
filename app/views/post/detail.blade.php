@extends("layout.master")

@section("page")
<div class="row">
    <div class="col-md-12">
        @if(isset($ad))
<!--        <h3>{{$ad->title}}</h3>-->
        <div>{{HTML::image($ad->image)}}</div>
        <p>{{$ad->description}}</p>
        <div class="row">
            <div class="col-md-6">Cost: {{$ad->cost}}</div>
            <div class="col-md-6">Seller: {{$ad->contact}}</div>
        </div>
        @endif

    </div>
</div>
@stop