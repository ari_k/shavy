<div class="col-xs-12 col-sm-5 col-sm-offset-1  col-md-5 col-md-offset-1 col-lg-3 col-lg-offset-1 well">
    <div class="row">

            <div class="col-xs-8 col-sm-9 col-md-8 col-lg-12">
                <h3><a href="{{URL::route('single_ad')}}">{{$post->get_name()}}</a></h3>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-4 col-lg-12 text-left text-danger">
                <h4>${{$post->get_cost()}}</h4>
            </div>


            <div class="col-md-12">
                <a href="{{URL::route('single_ad')}}">
                    {{HTML::Image($post->get_image(),$post->name,array("class"=>"img-responsive thumbnail"))}}
                </a>
            </div> 
            <br>
            <div class="col-md-12">
                @if(strlen($post->description)<100)
                <p>{{$post->description}}</p>
                @else
                <p>{{substr($post->description,0,100)}}...etc</p>
                @endif
            </div> 
            <br>
            <div class="col-xs-12 col-md-12">
                <p>
                    <strong><span class="text-danger">Seller:</span></strong> <em>{{$post->seller}}</em>
                </p>
            </div>
                        
    </div>
</div>
