<div>

    <div class="home_section">
        <h2>New Items</h2>

        <div>
            @include('home.new_items')
        </div>
    </div>
    <div class="home_section">
        <h2>Featured Items</h2>

        <div>
            @include('home.featured_items')
        </div>
    </div>
    <div class="home_section">
        <h2>Your Items</h2>

        <div>
            @include('home.your_items')
        </div>
    </div>
    <div class="home_section">
        <h2>Watched Items</h2>

        <div>
            @include('home.watched_items')
        </div>
    </div>

</div>