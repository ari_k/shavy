
    <ul class="nav nav-pills pull-right">
        <li class=@if(isset($active_home))"active"@endif>
            <a href="{{URL::route('homepage')}}">Home</a>
        </li>
        <li class=@if(isset($active_create_post))"active"@endif>
            <a href="{{URL::route('post_ad')}}">Post Ad</a>
        </li>
        <li class=@if(isset($active_browse_ads))"active"@endif>
            <a href="{{URL::route('view_ads')}}">Browse Ad</a>
        </li>
        <li><a href="#">
                @if(isset($username))
                {{ucfirst($username)}}
                @else
                Login
                @endif
            </a></li>
    </ul>
</nav>
<br>
<br>
<br>
