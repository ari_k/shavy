<?php


interface PostInterface{

    public function get_id();

    public function get_name();

    public function get_category();

    public function get_subcategory();

    public function get_seller();

    public function get_quantity();

    public function get_location();

    public function get_cost();

    public function get_date_posted();

    public function get_seller_name();

    public function get_seller_email();

    public function get_seller_phone();

    public function get_description();

    public function get_condition();

    public function get_status();

    public function get_views();

    public function add_view();

    public function get_watchlist();

    public function get_bid_count();

    public function get_feature();
}