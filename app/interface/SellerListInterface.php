<?php

interface SellerListInterface{

    public function get_id();

    public function get_seller();

    public function get_post();
}