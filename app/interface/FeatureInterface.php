<?php

interface FeatureInterface{

    public function get_id();

    public function get_name();

    public function get_level();

    public function get_price();
}