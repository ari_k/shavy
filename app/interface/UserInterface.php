<?php

interface UserInterface extends UnregisteredInterface{

    public function set_phone($phone);

    public function set_email($email);

    public function make($user_data);

    public function get_balance();

    public function add_balance($amount);

    public function remove_balance($amount);

    public function set_balance($amount);

    public function send_email($subject,$message);

    public function get_rating();
}