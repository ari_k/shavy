<?php


interface WatchedInterface{

    public function get_id();

    public function get_post();

    public function get_buyer();
}