<?php


interface UnregisteredInterface{

    public function get_id();

    public function get_email();

    public function get_phone();

    public function get_name();
}