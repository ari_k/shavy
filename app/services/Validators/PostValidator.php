<?php

class PostValidation extends Services\Validators\ValidatorBase
{
    public static $rules = array(
        'title' => 'required|min:1',
        //'category_id' => 'exist:categories,id',
        //'subcategory_id' => 'exist:subcategories,id',
        //'seller_email'=>'email',
        'quantity' => 'required|numeric',
        'cost' => 'required|numeric',
        'range' => 'required|numeric',
        //'seller_name' => 'required',
        //'seller_email' => 'required',
        //'seller_phone' => 'required',
        'description' => 'required|min:0|max:1000',
        //'condition' => 'required|exist:condition,id',
        //'status' => 'required|exist:status,id',
        ///'feature' => 'required|exist:required,id'
    );
    public static $message = array(

    );
}