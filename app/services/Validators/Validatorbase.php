<?php namespace Services\Validators;


abstract class ValidatorBase
{

    public $errors;
    protected $attributes;

    public function __construct($attributes = null)
    {
        $this->attributes = $attributes ? : \Input::all();
    }

    public function fails()
    {
        return !$this->passes() ? true : false;
    }

    public function passes()
    {
        $validation = \Validator::make($this->attributes, static::$rules, static::$message);

        if ($validation->passes()) return true;

        $this->errors = $validation->messages();

        return false;
    }

}