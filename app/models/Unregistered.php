<?php 

class Unresgistered extends Eloquent implements UnregisteredInterface{
	public function get_id(){
		return $this->id;
	}
	
    public function get_email(){
		return $this->email;
	}

    public function get_phone(){
		return $this->phone;
	}

    public function get_name(){
		return $this->name;
	}
}