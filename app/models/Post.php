<?php

class Post extends Eloquent implements PostInterface
{
    protected $table="post";
    protected $guarded=array('id');
    protected $default_image="http://webmuseum.mit.edu/mobiusicons/no_image.jpg";

    public function get_id()
    {
        return $this->id;
    }

    public function get_name()
    {
        return $this->title;
    }

    public function get_category()
    {
        return $this->belongsTo('category','category_id');
    }

    public function get_subcategory()
    {
        return $this->belongsTo('subcategory','subcategory_id');
    }

    public function get_seller()
    {
		return null;
        return $this->belongsTo('User', 'seller_id');
    }

    public function get_quantity()
    {
        return $this->quantity;
    }

    public function get_location()
    {
        //TODO needs work
        return null;
    }

    public function get_cost()
    {
        return $this->cost;
    }


    public function get_image(){
       return $this->image==''?$this->default_image:$this->image;
    }

    public function get_date_posted()
    {
        return $this->created_at;
    }

    public function get_seller_name()
    {
        $seller = $this->get_seller();
        if ($seller == null) {
            $seller_name = $this->seller_name;
        } else {
            $seller_name = $seller->name;
        }

        return $seller_name;
    }

    public function get_seller_email()
    {
        $seller = $this->get_seller();
        if ($seller == null) {
            $seller_email = $this->seller_email;
        } else {
            $seller_email = $seller->email;
        }
        return $seller_email;
    }

    public function get_seller_phone()
    {
        $seller = $this->get_seller();
        if ($seller == null) {
            $seller_phone = $this->seller_phone;
        } else {
            $seller_phone = $seller->phone;
        }
        return $seller_phone;
    }

    public function get_description()
    {
        return $this->description;
    }

    public function get_short_description($size=100){
        if(strlen($this->get_description())>$size){
            return substr($this->get_description(),0,$size);
        }else{
            return $this->get_description();
        }
    }

    public function get_condition()
    {
        return $this->belongsTo("condition","condition_id");
    }

    public function get_status()
    {
        return $this->belongsTo('status','status_id');
    }

    public function get_views()
    {
        $this->views;
    }

    public function add_view()
    {
        $view_count=$this->views;
        $view_count++;
        $this->views=$view_count;
        $this->save();
    }

    public function get_watchlist()
    {
        //TODO work to be done
        return null;
    }

    public function get_bid_count()
    {
        //TODO work to be done
    }

    public function get_feature()
    {
        $this->belongsTo('feature','feature_id');
    }
}