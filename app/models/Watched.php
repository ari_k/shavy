<?php


class Watched extends Eloquent implements WatchedInterface{
	
	public function get_id(){
	
		return $this->id;
	}

    public function get_post(){
	
		return $this->belongsTo('post','post_id');
	}

    public function get_buyer(){
		return $this->belongsTo('users','user_id');
	}

}