<?php

class User extends Eloquent implements UserInterface{

    protected $table='users';

    protected $guarded=array("balance");

    protected $hidden=array('password');

    public function get_id()
    {
        // TODO: Implement id() method.
    }

    public function name()
    {
        return $this->name;
    }

    public function phone()
    {
        return $this->phone;
    }

    public function email()
    {
        return $this->email;
    }

    public function make($user_data)
    {
        $user=User::create($user_data);
        return $user->save();
    }

    public function balance()
    {
        return $this->balance;
    }

    public function send_email($subject,$message)
    {
        // TODO: Implement send_email() method.
    }

    public function rating()
    {
        // TODO: Implement rating() method.
    }

    public function get_name()
    {
        // TODO: Implement get_name() method.
    }

    public function get_phone()
    {
        // TODO: Implement get_phone() method.
    }

    public function set_phone($phone)
    {
        // TODO: Implement set_phone() method.
    }

    public function get_email()
    {
        // TODO: Implement get_email() method.
    }

    public function set_email($email)
    {
        // TODO: Implement set_email() method.
    }

    public function get_balance()
    {
        // TODO: Implement get_balance() method.
    }

    public function add_balance($amount)
    {
        // TODO: Implement add_balance() method.
    }

    public function remove_balance($amount)
    {
        // TODO: Implement remove_balance() method.
    }

    public function set_balance($amount)
    {
        // TODO: Implement set_balance() method.
    }

    public function get_rating()
    {
        // TODO: Implement get_rating() method.
    }
}