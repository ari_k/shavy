<?php

class SellerList extends Eloquent implements SellerListInterface{

	public function get_id(){
		$this->id;
	}

    public function get_seller(){
		return $this->belongsTo('users','user_id')
	}

    public function get_post(){
		return $this->belongsTo('post','post_id');
	}
	
}